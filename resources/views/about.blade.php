@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


            <div class="card mt-3">
                <div class="card-header">
                   <h4>About Us</h4>
                </div>
                <div class="card-body">
                <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Harum odit animi vel natus doloribus,
                    quod eligendi explicabo aliquam neque omnis architecto distinctio
                    voluptates enim nisi fugit reiciendis, hic fuga ratione.
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati atque magni rerum sunt
                        sit enim libero aliquid earum autem nostrum,
                         molestias, totam quaerat aliquam quam, dolorum repellendus aperiam sed consequuntur?
                    </p>
                </p>
                </div>
                </div>
        </div>
    </div>
</div>

@stop
